#include <stdlib.h>
#include <stdio.h>

void writeChars(char *ptr)
{
    for(int i = 0; i <  100000000; i++)
        printf("%c\n",ptr[i]);
}

void readChars(char *ptr)
{
    for(int i = 0; i <  100000000; i++)
       ptr[i] = 'a';
}


int main()
{

    char *ptr;

    ptr = (char*)malloc(sizeof(char)*100000000);

    readChars(ptr);
    writeChars(ptr);

    printf("%lu\n",sizeof(ptr));
}