/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ahorcado.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 11:09:19 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/10 11:09:23 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

char	*ft_strcpy(char *dest, char *src)
{
	int i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int ft_strlen(char *str){
    int i;

    i = 0;
    while(str[i] != '\0')
        i++;
    return (i);
}

void ft_putchar(char a)
{
    write(1,&a,1);
}

void ft_putstr(char *str)
{
    int i;

    i = 0;
    while(str[i])
    {
        ft_putchar(str[i]);
        i++;
    }
}

void createWord(char* objetivo, char *jugador)
{
    int nbr_words = 3;
    int random = 1;
    int len;
    char palabras[3][10] = {
        "patata",
        "leon",
        "espada"
    };

    len = ft_strlen(palabras[random]);
    objetivo = &(char*)malloc(sizeof(char)*len);
    ft_strcpy(objetivo, palabras[random]);
    jugador = &(char*)malloc(sizeof(char)*len);
    int i;
	i = 0;
	while (jugador[i] != '\0')
	{
		jugador[i] = '-';
		i++;
	}
	jugador[i] = '\0';
}



int     main()
{
    int continuar;
    char *palabra;
    char *jugador;
    createWord(palabra,jugador);
    continuar = 1;
    ft_putstr(palabra);
    ft_putstr("\n");
    ft_putstr(jugador);
   

}

