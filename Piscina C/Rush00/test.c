/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/28 12:09:26 by agracia-          #+#    #+#             */
/*   Updated: 2020/11/28 12:30:23 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<unistd.h>

int 	main(void){
	
  rush01(5,5);

}


void	ft_putchar(char c){

	write(1, &c,1);

}


void	rush01(int x, int y){

	//Contadores para seguir el dibujo del cuadrado
 	//count_x avanzamos por el eje x count_y avanzamos por el eje y
	int count_x;
 	int count_y;

	
	
 	count_x = 0;
	count_y = 0;
  
  
	while(count_x < x){
		while(count_y < y){

			ft_putchar('*');
			count_y++;
		}

		count_y=0;

		count_x++;
		ft_putchar('\n');
	}

}


