/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   programa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pgomez-a <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/28 12:07:29 by pgomez-a          #+#    #+#             */
/*   Updated: 2020/11/28 19:57:59 by pgomez-a         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	rush(int x, int y)
{
	int cy;
	int cx;

	cx = 0;
	cy = 0;
	while (cy < y)
	{
		while (cx < x)
		{
			if ((cx == 0 && cy == 0)
				|| (cx == x - 1 && cy == y - 1 && cy != 0 && cx != 0))
				ft_putchar('/');
			else if ((cx == x - 1 && cy == 0) || (cy == y - 1 && cx == 0))
				ft_putchar('\\');
			else if (cx == 0 || cy == 0 || cx == x - 1 || cy == y - 1)
				ft_putchar('*');
			else
				ft_putchar(' ');
			cx++;
		}
		cx = 0;
		cy++;
		ft_putchar('\n');
	}
}
