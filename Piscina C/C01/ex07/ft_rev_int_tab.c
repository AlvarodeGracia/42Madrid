/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 21:25:51 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/01 21:25:54 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_rev_int_tab(int *tab, int size)
{
	int i;
	int aux;
	int last;

	i = 0;
	last = size - 1;
	while (i < size / 2)
	{
		aux = tab[last - i];
		tab[last - i] = tab[i];
		tab[i] = aux;
		i++;
	}
}
