/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 22:00:06 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/01 22:00:09 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{
	int i;
	int z;
	int aux;

	i = 0;
	while (i < size - 1)
	{
		z = i + 1;
		while (z < size)
		{
			if (tab[i] > tab[z])
			{
				aux = tab[i];
				tab[i] = tab[z];
				tab[z] = aux;
			}
			z++;
		}
		i++;
	}
}
