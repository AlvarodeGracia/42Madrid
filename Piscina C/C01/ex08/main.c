/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/01 22:01:51 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/01 22:01:54 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <unistd.h>

void ft_sort_int_tab(int *tab, int size);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nbr)
{
	if (nbr < 0)
	{
		ft_putchar('-');
		if (nbr == -2147483648)
		{
			ft_putchar('2');
			nbr = -147483648;
		}
		nbr = nbr * -1;
	}
	if (nbr >= 10)
	{
		ft_putnbr(nbr / 10);
		ft_putchar(nbr % 10 + '0');
	}
	else
	{
		ft_putchar(nbr + '0');
	}
}

void ft_print_array_nbr(int * nbrs, int size)
{
    int i = 0;
    while(i < size)
    {
        ft_putnbr(nbrs[i]);
        i++;
    }
}

int main(void)
{
    int nbrs[] = {5,7,3,5,2,8,4,6,3,4};
    int size = 10;
    ft_sort_int_tab(nbrs, size);

    ft_print_array_nbr(nbrs, size);
        
}
