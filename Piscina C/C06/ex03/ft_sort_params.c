/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/16 21:00:27 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 14:00:40 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		ft_strcmp(char *s1, char *s2)
{
	int diference;
	int i;

	diference = 0;
	i = 0;
	while (s1[i] != '\0' && s2[i] != '\0' && s1[i] == s2[i])
		i++;
	diference = s1[i] - s2[i];
	return (diference);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
}

void	ft_put_array_str(char **str, int size)
{
	int nbr;

	nbr = 1;
	while (nbr < size)
	{
		ft_putstr(str[nbr]);
		ft_putstr("\n");
		nbr++;
	}
}

int		main(int argc, char **argv)
{
	int		nbr;
	int		next;
	char	*aux;

	nbr = 1;
	while (nbr < argc)
	{
		next = nbr + 1;
		while (next < argc)
		{
			if (ft_strcmp(argv[nbr], argv[next]) > 0)
			{
				aux = argv[nbr];
				argv[nbr] = argv[next];
				argv[next] = aux;
			}
			next++;
		}
		nbr++;
	}
	ft_put_array_str(argv, argc);
}
