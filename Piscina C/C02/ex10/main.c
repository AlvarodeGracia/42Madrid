/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 18:11:34 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 18:11:37 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdio.h>
#include <string.h>

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int main()
{
    char src[] = "";
    char dest[10];

    /* count less than length of src */
    int size = strlcpy(dest,src,10);
    printf("%d\n",size);
    printf("%s\n",dest);
    printf("*********\n");
    size = ft_strlcpy(dest,src,10);
    printf("%d\n",size);
    printf("%s\n",dest);

    return 0;
}
