/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:22:52 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 15:22:54 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>

char	*ft_strupcase(char *str);

int main()
{
    char str[ ] = "Modify This String To Upper";
    printf("%s\n",ft_strupcase(str));
    return  0;
}