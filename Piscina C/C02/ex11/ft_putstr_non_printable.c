/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 18:33:55 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 18:33:57 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putstr_non_printable(char *str)
{
	int				i;
	unsigned char	h;
	char			*base;

	base = "0123456789abcdef";
	i = 0;
	while (1)
	{
		h = str[i];
		if (h == '\0')
		{
			break ;
		}
		if (h < 32 || h > 126)
		{
			write(1, "\\", 1);
			write(1, &base[h / 16], 1);
			write(1, &base[h % 16], 1);
		}
		else
		{
			write(1, &h, 1);
		}
		i++;
	}
}
