/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:47:01 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 15:47:02 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int i;
	int isalpha;

	i = 0;
	while (str[i] != 0)
	{
		isalpha = 0;
		if (str[i - 1] >= 'a' && str[i - 1] <= 'z')
			isalpha = 1;
		else if (str[i - 1] >= 'A' && str[i - 1] <= 'Z')
			isalpha = 1;
		else if (str[i - 1] >= '0' && str[i - 1] <= '9')
			isalpha = 1;
		if ((!isalpha || i == 0) && str[i] >= 'a' && str[i] <= 'z')
			str[i] += 'A' - 'a';
		if ((isalpha) && str[i] >= 'A' && str[i] <= 'Z')
			str[i] += 'a' - 'A';
		i++;
	}
	return (str);
}
