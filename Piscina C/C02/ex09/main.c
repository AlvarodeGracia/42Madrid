/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:47:11 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 15:47:12 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include<stdio.h>
#include<string.h>

char	*ft_strcapitalize(char *str);

int main()
{
    char str[ ] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
    printf("%s\n",ft_strcapitalize(str));
    return  0;
}