/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/07 13:29:35 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/07 13:29:36 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include<stdio.h>
#include<string.h>

int	ft_str_is_printable(char *str);

int main()
{
    char str[ ] = "ASD ";
    printf("%d",ft_str_is_printable(str));
    return  0;
}