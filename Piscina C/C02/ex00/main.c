/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 20:46:04 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/02 20:46:06 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdio.h>
#include <string.h>

void ft_strcpy(char* dest, char* src);

int main() {
   char str1[20] = "C programming";
   char str2[20];

   // copying str1 to str2
   ft_strcpy(str2, str1);
   //strcpy(str2, str1);
   puts(str1);
   puts(str2); // C programming

   return 0;
}