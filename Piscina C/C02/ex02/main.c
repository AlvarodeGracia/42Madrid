/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 21:42:06 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/02 21:42:08 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include<stdio.h>
#include<string.h>

int	ft_str_is_alpha(char *str);

int main()
{
    char str[ ] = "ModifyTisStringToUpper";
    printf("%d",ft_str_is_alpha(str));
    return  0;
}