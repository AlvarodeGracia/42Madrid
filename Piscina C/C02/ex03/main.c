/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/02 22:22:39 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/02 22:22:40 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>

int	ft_str_is_numeric(char *str);

int main()
{
    char str[ ] = "123A";
    printf("%d",ft_str_is_numeric(str));
    return  0;
}
