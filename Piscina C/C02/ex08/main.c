/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/03 15:44:06 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/03 15:44:07 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include<stdio.h>
#include<string.h>

char	*ft_strlowcase(char *str);

int main()
{
    char str[ ] = "Modify This String To Upper";
    printf("%s\n",ft_strlowcase(str));
    return  0;
}
