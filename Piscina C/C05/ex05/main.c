/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 19:27:02 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 16:36:10 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */



#include <stdio.h>

int		ft_sqrt(int nb);

int		ft_recursive_power(int nb, int power)
{
	if (power < 0)
		return (0);
	if (power == 0 && nb != 0)
		return (1);
	if (power > 1)
		nb *= ft_recursive_power(nb, power - 1);
	return (nb);
}

int main(void)
{
    printf("%d\n",ft_sqrt(2147395601));
    return(0);
}