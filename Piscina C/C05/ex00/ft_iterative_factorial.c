/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 19:23:01 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 17:43:14 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int resultado;
	int i;

	if (nb < 0)
		return (0);
	resultado = 1;
	i = 1;
	while (i <= nb)
	{
		resultado *= i;
		i++;
	}
	return (resultado);
}
