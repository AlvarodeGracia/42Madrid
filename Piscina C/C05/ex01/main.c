/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 19:22:53 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 17:45:37 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_recursive_factorial(int nb);

int main(void)
{

    for(int i = -5; i <= 15; i++)
        printf("%d - %d\n",i, ft_recursive_factorial(i));
    return(0);
}