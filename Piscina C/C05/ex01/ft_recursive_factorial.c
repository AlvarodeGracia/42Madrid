/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 19:22:56 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 17:46:10 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_factorial(int nbr)
{
	if (nbr == 0)
		nbr = 1;
	else if (nbr < 0)
		nbr = 0;
	else if (nbr > 1)
		nbr *= ft_recursive_factorial(nbr - 1);
	return (nbr);
}
