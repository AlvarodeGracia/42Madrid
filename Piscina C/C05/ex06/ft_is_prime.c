/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/15 19:27:00 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/17 15:56:29 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_prime(int nb)
{
	int resultado;
	int i;

	if (nb < 2)
		return (0);
	i = nb - 1;
	resultado = 1;
	while (i > 1)
	{
		if (nb % i == 0)
			resultado = 0;
		i--;
	}
	return (resultado);
}
