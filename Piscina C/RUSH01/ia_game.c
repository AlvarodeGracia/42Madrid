/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ia_game.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgomez-d <lgomez-d@student.42lyon.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 19:27:57 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/06 15:37:05 by lgomez-d         ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
/*
    a.out "4 3 2 1 1 2 2 2 4 3 2 1 1 2 2 2"

     -4-3-2-1-
    4-1-2-3-4-1
    3-2      -2
    4-3      -2
    1-4-   -3-2
     -1-2-2-2-
*/

void establecer_valor(int valor, int x,int y,int tabla[4][4][5]);
int fila_tiene_x(int tabla[4][4][5], int fila, int valor, int yo);
int columna_tiene_x(int tabla[4][4][5], int columna, int valor, int yo);
void desactivar_valor(int casilla[5], int valor);
int por_descarte_fila(int tabla[4][4][5], int y, int x);
int por_descarte_columna(int tabla[4][4][5], int y, int x);
int casilla_es_valor(int *casilla, int valor);


int casilla_es_4(int x, int y, int cabeceras[4][4], int tabla[4][4][5] )
{
    int cambios;
    cambios = 0;
    if(
        ((y == 0) && cabeceras[0][x] == 1) ||
        ((y == 3) && cabeceras[1][x] == 1) ||
        ((x == 0) && cabeceras[2][y] == 1) ||
        ((x == 3) && cabeceras[3][y] == 1))
        {
            establecer_valor(4, x ,y, tabla);
            cambios++;
        }
    return (cambios);

}

int casilla_es_123(int x, int y, int cabeceras[4][4], int tablero[4][4][5])
{
    int cambios;
    
    cambios = 0;
    if (cabeceras[0][x] == 4)
    {
        establecer_valor(y + 1, x, y, tablero);
        cambios++;
    }
    if (cabeceras[1][x] == 4)
    {
        establecer_valor((y - 4) * - 1, x, y, tablero);
        cambios++;
    }
    if (cabeceras[2][y] == 4)
    {
        
        establecer_valor(x + 1, x, y, tablero);
        cambios++;
    }
    if (cabeceras[3][y] == 4)
    {
        establecer_valor((x - 4) * - 1, x, y, tablero);
        cambios++;
    }

    return (cambios);
}

int esquina2(int x, int y, int cabeceras[4][4], int tabla[4][4][5])
{
    int cambios;
    cambios = 0;

    if((x == 0 || x == 3) && ((y == 0) || y == 3))
    {
        if(cabeceras[1][x] == 3 && cabeceras[3][y] == 2)
        {
            printf("escribir esquina\n");
            establecer_valor(2, x, y, tabla);
        }
    }

    return (cambios);
}

int cabeceras_2_1(int x, int y, int cabeceras[4][4], int tabla[4][4][5])
{
    int cambios;

    int cambio = 0;

    if (y == 3 && cabeceras[0][x] == 1 && cabeceras[1][x] == 2)
        cambio = 1;
    else if (y == 0 && cabeceras[0][x] == 2 && cabeceras[1][x] == 1)
        cambio = 1;
    else  if (x == 3 && cabeceras[2][y] == 1 && cabeceras[3][y] == 2)
        cambio = 1;
    if (x == 0 && cabeceras[2][y] == 2 && cabeceras[3][y] == 1)
        cambio = 1;

    if(cambio == 1)
    {
        establecer_valor(3, x, y, tabla);
        cambios++;
    }
    return (cambios);
}




int por_descarte(int x, int y, int tabla[4][4][5]){

    int cambios;
    cambios = 0;

    cambios += por_descarte_fila(tabla, y, x);
    cambios += por_descarte_columna(tabla, y, x);

    return (cambios);
}

int establecer_3(int x, int y, int cabeceras[4][4], int tabla[4][4][5])
{
	int cambios;
	int posicion;

	cambios = 0;

	if ((y == 0 || y == 1) && cabeceras[0][x] == 3 && casilla_es_valor(tabla[2][x], 4) == 1 && casilla_es_valor(tabla[3][x], 3) == 1)
	{
		posicion = y + 1;

		establecer_valor(y+1, x, y, tabla);
		cambios++;
	}
	if ((y == 3 || y == 2) && cabeceras[1][x] == 3 && casilla_es_valor(tabla[1][x], 4) == 1 && casilla_es_valor(tabla[0][x], 3) == 1)
	{
		posicion = (y - 4) * -1;
		establecer_valor(((y-4)*-1)  , x, y, tabla);
		cambios++;
	}
	if ((x == 0 || x == 1) && cabeceras[2][y] == 3 && casilla_es_valor(tabla[y][2], 4) == 1 && casilla_es_valor(tabla[y][3], 3) == 1)
	{
		posicion = y + 1;
		establecer_valor(y + 1, x, y, tabla);
		cambios++;
	}
	if ((x == 3 || x == 2) && cabeceras[3][y] == 3 && casilla_es_valor(tabla[y][1], 4) == 1 && casilla_es_valor(tabla[y][0], 3) == 1)
	{
		posicion = (y - 4) * -1;
		establecer_valor(((y - 4) * -1), x, y, tabla);
		cambios++;
	}
	if ((x == 3 || x == 2) && cabeceras[3][y] == 3 && casilla_es_valor(tabla[y][1], 4) == 1 && casilla_es_valor(tabla[y][0], 3) == 1)
	{
		posicion = (y - 4) * -1;
		establecer_valor(((y - 4) * -1), x, y, tabla);
		cambios++;
	}
	if (cambios > 0)
	{
		establecer_valor(y + 1, x, y, tabla);
	}
	establecer_valor(y + 1, x, y, tabla);
	return (cambios);
}

void	poner_uno(int tabla[4][4][5])
{
	int y;
	int x;
	int exit;

	y = 0;
	x = 0;
	exit = 0;
	while (y < 4 && exit == 0)
	{
		while (x < 4 && exit == 0)
		{
			if (tabla[y][x][0] == 0)
			{
				establecer_valor(2, x, y, tabla);
				exit = 1;
			}
			x++;
		}
		x = 0;
		y++;
	}
}
