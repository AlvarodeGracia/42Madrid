/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_casilla.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/06 19:43:03 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/06 19:43:05 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	desactivar_valor(int casilla[5], int valor)
{
	casilla[valor] = 0;
	if ((casilla[1] + casilla[2] + casilla[3] + casilla[4]) == 1)
		casilla[0] = 1;
}

void	establecer_valor(int valor, int x, int y, int tabla[4][4][5])
{
	int i;

	tabla[y][x][0] = 1;
	tabla[y][x][1] = 0;
	tabla[y][x][2] = 0;
	tabla[y][x][3] = 0;
	tabla[y][x][4] = 0;
	tabla[y][x][valor] = 1;
	i = 0;
	while (i < 4)
	{
		if (i != x && tabla[y][i][0] == 0)
			desactivar_valor(tabla[y][i], valor);
		i++;
	}
	i = 0;
	while (i < 4)
	{
		if (i != y && tabla[i][x][0] == 0)
			desactivar_valor(tabla[i][x], valor);
		i++;
	}
}

int		casilla_es_valor(int *casilla, int valor)
{
	int es_valor;

	es_valor = 0;
	if ((casilla[1] + casilla[2] + casilla[3] + casilla[4]) == 1)
		if (casilla[valor] == 1)
			es_valor = 1;
	return (es_valor);
}

int		por_descarte_fila(int tabla[4][4][5], int y, int x)
{
	int validos;
	int valor;
	int pos_fil;
	int cambios;

	valor = 0;
	pos_fil = 0;
	cambios = 0;
	while (valor <= 4)
	{
		while (pos_fil < 4)
		{
			if (pos_fil != x && tabla[y][pos_fil][valor] == 0)
				validos++;
			pos_fil++;
		}
		if (validos == 3)
		{
			establecer_valor(valor, x, y, tabla);
			cambios++;
		}
		validos = 0;
		pos_fil = 0;
		valor++;
	}
	return (cambios);
}

int		por_descarte_columna(int tabla[4][4][5], int y, int x)
{
	int validos;
	int valor;
	int pos_col;
	int cambios;

	valor = 0;
	pos_col = 0;
	cambios = 0;
	while (valor <= 4)
	{
		while (pos_col < 4)
		{
			if (pos_col != y && tabla[pos_col][x][valor] == 0)
				validos++;
			pos_col++;
		}
		if (validos == 3)
		{
			establecer_valor(valor, x, y, tabla);
			cambios++;
		}
		validos = 0;
		pos_col = 0;
		valor++;
	}
	return (cambios);
}
