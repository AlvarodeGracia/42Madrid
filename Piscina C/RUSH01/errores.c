/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errores.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 12:34:35 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/05 12:34:38 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putstr(char *str);
int		contar_caracteres(char *cadena);

int		is_numero_space(char *str)
{
	int r;
	int is_espacio;
	int i;

	r = 1;
	is_espacio = 1;
	i = 0;
	while (str[i] != '\0')
	{
		if (is_espacio < 0 && str[i] != ' ')
		{
			r = 0;
			break ;
		}
		else if (is_espacio > 0 && !(str[i] <= '4' && str[i] >= '1'))
		{
			r = 0;
			break ;
		}
		is_espacio *= -1;
		i++;
	}
	return (r);
}

int		controlar_errores(int argc, char **argv)
{
	int is_ok;

	is_ok = 1;
	if (argc != 2)
		is_ok = 0;
	else
	{
		if (contar_caracteres(argv[1]) != 16 * 2 - 1)
			is_ok = 0;
		if (is_numero_space(argv[1]) == 0)
			is_ok = 0;
	}
	return (is_ok);
}
