/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   factoria_casillas.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 13:13:17 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/05 13:13:18 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	create(int x, int y, int tablero[4][4][5])
{
	int i;
	int z;

	i = 0;
	z = 0;
	while (i < y)
	{
		while (z < x)
		{
			tablero[i][z][0] = 0;
			tablero[i][z][1] = 1;
			tablero[i][z][2] = 1;
			tablero[i][z][3] = 1;
			tablero[i][z][4] = 1;
			z++;
		}
		z = 0;
		i++;
	}
}

int		cargar_datos(char *cadena, int *cabecera, int start, int end)
{
	int i;
	int z;

	i = start;
	z = 0;
	while (i < end)
	{
		cabecera[z] = cadena[i] - 48;
		i += 2;
		z++;
	}
	return (i);
}
