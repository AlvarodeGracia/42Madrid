/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vista.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/05 15:12:44 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/05 15:12:48 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nbr);

void	print_array(int x, int y, int tablero[4][4][5])
{
	int i;
	int z;

	i = 0;
	z = 0;
	while (i < y)
	{
		while (z < x)
		{
			if (tablero[i][z][0] == 0)
				write(1, "0", 1);
			else if (tablero[i][z][1] == 1)
				write(1, "1", 1);
			else if (tablero[i][z][2] == 1)
				write(1, "2", 1);
			else if (tablero[i][z][3] == 1)
				write(1, "3", 1);
			else if (tablero[i][z][4] == 1)
				write(1, "4", 1);
			else
				write(1, "-", 1);
			z++;
			// write(1, " ", 2);
		}
		write(1, "\n", 1);
		z = 0;
		i++;
	}
}

void	print_cabecera(int size, int *nbrs)
{
	int i;

	i = 0;
	while (i < size)
	{
		ft_putnbr(nbrs[i]);
		write(1, " ", 1);
		i++;
	}
	write(1, "\n", 1);
}
