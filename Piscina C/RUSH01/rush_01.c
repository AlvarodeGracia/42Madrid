/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush-01.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/06 22:28:11 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/06 22:28:13 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	controlar_errores(int argc, char **argv);
void create(int x, int y, int tablero[4][4][5]);
void print_array(int x, int y, int tablero[4][4][5]);
void print_cabecera(int size, int *nbrs);
int cargar_datos(char *cadena, int *colup, int start, int end);
void print_cabecera(int size, int *nbrs);
int casilla_es_4(int y, int x, int cabeceras[4][4], int tabla[4][4][5]);
int casilla_es_123(int x, int y, int cabeceras[4][4], int tabla[4][4][5]);
int esquina2(int x, int y, int cabeceras[4][4], int tabla[4][4][5]);
int cabeceras_2_1(int x, int y, int cabeceras[4][4], int tabla[4][4][5]);
int por_descarte(int x, int y, int tabla[4][4][5]);
int poner_uno(int tabla[4][4][5]);
int establecer_3(int x, int y, int cabeceras[4][4], int tabla[4][4][5]);

void	cargar_datos_iniciales(char *cadena, int cabeceras[4][4])
{
	int start;

	start = 0;
	start = cargar_datos(cadena, cabeceras[0], start, 8);
	start = cargar_datos(cadena, cabeceras[1], start, 8 * 2);
	start = cargar_datos(cadena, cabeceras[2], start, 8 * 3);
	start = cargar_datos(cadena, cabeceras[3], start, 8 * 4);
}

int		preguntas(int x, int y, int cabeceras[4][4], int tablero[4][4][5])
{
	int cmb;

	cmb = 0;
	cmb += casilla_es_4(x, y, cabeceras, tablero);
	cmb += casilla_es_123(x, y, cabeceras, tablero);
	cmb += cabeceras_2_1(x, y, cabeceras, tablero);
	cmb += por_descarte(x, y, tablero);
	return (cmb);
}

int		resolver(int tablero[4][4][5], int cabeceras[4][4])
{
	int y;
	int x;
	int cmb;

	y = 0;
	x = 0;
	cmb = 0;
	while (y < 4)
	{
		while (x < 4)
		{
			if (tablero[y][x][0] != 1)
			{
				cmb = preguntas(x, y, cabeceras, tablero);
			}
			x++;
		}
		x = 0;
		y++;
	}
	return (cmb);
}

char * d = "dfsadfsadf"
xchar ** {
	"programa",
	"asdfsdf",
	"dsafsdf"

}

int	main(int argc, char **argv)
{
	int tablero[4][4][5];
	int cabeceras[4][4];
	int continuar;

	continuar = 1;
	if (controlar_errores(argc, argv) != 0)
	{
		cargar_datos_iniciales(argv[1], cabeceras);
		create(4, 4, tablero);
		while (continuar == 1)
		{
			if (resolver(tablero, cabeceras) == 0)
				continuar = 0;
		}
		print_array(4, 4, tablero);
	}
	return (0);
}
