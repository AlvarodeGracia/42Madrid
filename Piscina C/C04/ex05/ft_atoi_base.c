/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 20:15:17 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/10 20:15:21 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int ft_atoi_base(char *str, char *base)
{
	int resultado;
	int is_positivo;

	resultado = 0;
	is_positivo = 0;
	while ((*str >= '\t' && *str <= '\r') || *str == ' ')
		str++;
	while (*str == '+' || *str == '-')
	{
		if (*str == '-')
			is_positivo++;
		str++;
	}
	while (*str >= '0' && *str <= '9')
	{
		if (resultado > 0)
			resultado *= 10;
		resultado += *str - '0';
		str++;
	}
	if (is_positivo % 2 != 0 && resultado != 0)
		resultado *= -1;
	return (resultado);
}
