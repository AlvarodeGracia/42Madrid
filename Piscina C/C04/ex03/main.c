/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 12:46:39 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/14 20:34:46 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int		ft_atoi(char *str);

int main () {

    printf("%d\n",  ft_atoi("123456"));
    printf("%d\n",  ft_atoi("123Three45678"));
    printf("%d\n",  ft_atoi("Hello world"));
    printf("%d\n",  ft_atoi("+42 BLAH!"));
    printf("%d\n",  ft_atoi("-42"));
    printf("%d\n",  ft_atoi("         +42"));
    printf("%d\n",  ft_atoi("\t\n\v\f\r 42"));
    printf("%d\n",  ft_atoi("123456"));
    printf("%d\n",  ft_atoi(" ---+--+1234ab567"));
    printf("%d\n",  ft_atoi(" --a-+--+1234ab567"));
    printf("%d\n",  ft_atoi("\t\ns\v\f\r 42"));
    printf("***\n");
    printf("%d\n",  atoi("123456"));
    printf("%d\n",  atoi("123Three45678"));
    printf("%d\n",  atoi("Hello world"));
    printf("%d\n",  atoi("+42 BLAH!"));
    printf("%d\n",  atoi("-42"));
    printf("%d\n",  atoi("         +42"));
    printf("%d\n",  atoi("\t\n\v\f\r 42"));
    printf("%d\n",  atoi("123456"));
    printf("%d\n",  atoi(" ---+--+1234ab567"));
    printf("%d\n",  atoi(" --a-+--+1234ab567"));
    printf("%d\n",  ft_atoi("\t\ns\v\f\r 42"));
    printf("***\n");

    printf("%d\n",  ft_atoi("-132fasdfasdf5632167"));
    printf("%d\n",  ft_atoi("-oks"));
    printf("%d\n",  ft_atoi("13fasddfaasd\tdf25632asfdas"));
    printf("%d\n",  ft_atoi("5807fasdfassdassdfa64829"));
    printf("%d\n",  ft_atoi("21474ssdfas83648234"));
    printf("%d\n",  ft_atoi("-214dfasd7483647"));

    printf("%d\n",  ft_atoi("2147483648"));
    printf("%d\n",  atoi("2147483648"));

   return(0);
}
