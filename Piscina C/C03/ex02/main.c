/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/07 16:08:24 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/07 16:08:27 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

char	*ft_strcat(char *dest, char *src);

int main ()
{
  char str[80];
  strcpy (str,"these ");
  ft_strcat (str,"strings ");
  ft_strcat (str,"are ");
  ft_strcat (str,"concatenated.");
  puts (str);
  return 0;
}