/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/07 16:08:24 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/07 16:08:27 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* strstr example */
#include <stdio.h>
#include <string.h>

char	*ft_strstr(char *str, char *to_find);

int main ()
{
  char str[] ="This is a simple string";
  char * pch;
  pch = ft_strstr (str,"simple");
  if (pch != NULL)
    strncpy (pch,"sample",6);
  puts (str);
  return 0;
}
