/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/07 16:08:24 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/07 16:08:27 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* strncat example */
#include <stdio.h>
#include <string.h>

char	*ft_strncat(char *dest, char *src, unsigned int nb);


int main ()
{
  char str1[20];
  char str2[20];
  strcpy (str1,"To be ");
  strcpy (str2,"or not to be");
  ft_strncat (str1, str2, 6);
  puts (str1);
  return 0;
}