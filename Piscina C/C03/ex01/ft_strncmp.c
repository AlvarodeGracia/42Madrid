/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/08 14:44:23 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/08 14:44:25 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int i;
	unsigned int diferencia;

	diferencia = 0;
	i = 0;
	if (n != 0)
	{
		while (s1[i] && s2[i] && s1[i] == s2[i] && (i < (n - 1)))
			i++;
		diferencia = s1[i] - s2[i];
	}
	return (diferencia);
}
