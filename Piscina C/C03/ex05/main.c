/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/07 16:08:24 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/07 16:08:27 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);

int main(void)
{
    char str[40];
    int r;

    strcpy (str,"12345");

    r = ft_strlcat(str,"67890",3); //10   //8
    printf("r1=%d\n",r);
    puts (str);

    r = ft_strlcat(str,"12345",15); //10
    printf("r2=%d\n",r);
    puts (str);

    r = ft_strlcat(str,"concatenated.",15);  //16
    printf("r3=%d\n",r);
    puts (str);


    char test[256] = "\0zxcvzxcvzxcvxzcvzxcv";
    printf("%d-", ft_strlcat(test, "asdf", 16));
    printf("%s\n", test);
    printf("%d-", ft_strlcat(test, "asdf", 6));
    printf("%s\n", test);
    printf("%d-", ft_strlcat(test, "asdf", 4));
    printf("%s\n", test);
    printf("%d-", ft_strlcat(test, "", 16));
    printf("%s\n", test);
    printf("%d-", ft_strlcat(test, "asdf", 0));
    printf("%s\n", test);

    return 0;
}