/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 22:07:56 by agracia-          #+#    #+#             */
/*   Updated: 2020/12/09 22:19:51 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	unsigned int resultado;
	unsigned int i;
	unsigned int z;

	resultado = 0;
	i = 0;
	z = 0;
	while (dest[i] != '\0')
		i++;
	if (size < i)
		resultado += size;
	else
		resultado += i;
	while (src[z] != '\0')
	{
		if (i < (size - 1))
		{
			dest[i] = src[z];
			i++;
		}
		z++;
	}
	dest[i] = '\0';
	return (resultado + z);
}
