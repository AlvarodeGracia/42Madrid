/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agracia- <agracia-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/22 19:54:55 by agracia-          #+#    #+#             */
/*   Updated: 2021/01/22 19:57:08 by agracia-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*ndst;
	const char	*nsrc;

	ndst = (char *)dst;
	nsrc = (char *)src;
	if (dst > src)
	{
		while (len--)
			ndst[len] = nsrc[len];
	}
	else if (dst < src)
		ft_memcpy(dst, src, len);
	return (dst);
}
